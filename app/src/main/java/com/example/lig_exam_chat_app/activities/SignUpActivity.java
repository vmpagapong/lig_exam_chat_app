package com.example.lig_exam_chat_app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.lig_exam_chat_app.R;
import com.example.lig_exam_chat_app.models.User;
import com.example.lig_exam_chat_app.utils.Debugger;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class SignUpActivity extends AppCompatActivity {

    private Context context;

    private EditText etUsername, etPassword;
    private TextView tvUsernameError, tvPasswordError, tvLogin;
    private Button btnSignup;
    private ProgressBar progressBar;

    private FirebaseAuth firebaseAuth;

    private String username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        context = this;

        firebaseAuth = FirebaseAuth.getInstance();

        initializeUI();
    }

    private void initializeUI() {
        etUsername = findViewById(R.id.et_signup_username);
        etPassword = findViewById(R.id.et_signup_password);
        tvUsernameError = findViewById(R.id.tv_signup_username_error);
        tvPasswordError = findViewById(R.id.tv_signup_password_error);
        tvLogin = findViewById(R.id.tv_signup_login);
        btnSignup = findViewById(R.id.btn_signup_signup);
        progressBar = findViewById(R.id.progressBar_signup);

        etUsername.requestFocus();

        etUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showError(false);
            }
        });

        etPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showError(false);
            }
        });

        tvLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = etUsername.getText().toString();
                password = etPassword.getText().toString();

                if (!areFieldsEmpty()) {
                    if (isLengthValid())
                        doSingUp();
                    else
                        showError(true);
                } else
                    showError(true);
            }
        });
    }

    private void showProgress(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        btnSignup.setVisibility(show ? View.GONE : View.VISIBLE);
        etUsername.setEnabled(!show);
        etPassword.setEnabled(!show);
        tvLogin.setEnabled(!show);
    }

    private void showError(boolean show) {
        tvUsernameError.setVisibility(show ? View.VISIBLE : View.GONE);
        tvPasswordError.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private boolean isLengthValid() {
        if (username.length() >= 8 && username.length() <= 16) {
            if (password.length() >= 8 && password.length() <= 16) {
                return true;
            }
        }

        return false;
    }

    private boolean areFieldsEmpty() {
        return username.isEmpty()
                && password.isEmpty();
    }

    private void doSingUp() {
        showProgress(true);
        firebaseAuth.signInAnonymously().addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                showProgress(false);
                if (task.isSuccessful()) {
                    FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                    assert firebaseUser != null;
                    String userId = firebaseUser.getUid();
                    DatabaseReference userRefs = FirebaseDatabase.getInstance().getReference("Users").child(userId);

                    HashMap<String, Object> hashMap = new HashMap<>();
                    hashMap.put("id", userId);
                    hashMap.put("username", username);
                    hashMap.put("password", password);

                    userRefs.setValue(hashMap);

                    User user = new User();
                    user.setId(userId);
                    user.setUsername(username);
                    user.setPassword(password);
                    user.saveUserSession(context);

                    Intent intent = new Intent(context, ChatActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                } else {
                    Debugger.printO(task);
                    Toast.makeText(context, "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
