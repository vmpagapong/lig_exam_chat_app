package com.example.lig_exam_chat_app.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lig_exam_chat_app.MainActivity;
import com.example.lig_exam_chat_app.R;
import com.example.lig_exam_chat_app.adapters.ChatAdapter;
import com.example.lig_exam_chat_app.models.Chat;
import com.example.lig_exam_chat_app.models.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class ChatActivity extends AppCompatActivity {

    private Context context;

    private RecyclerView recyclerView;
    private EditText etMessage;
    private Button btnSend;

    private String message;
    private String username;
    private String userId;

    private FirebaseAuth firebaseAuth;
    private DatabaseReference chatRef;
    private ValueEventListener chatListener;

    private ChatAdapter chatAdapter;
    private ArrayList<Chat> chatArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        context = this;

        checkUserSession(false);

        firebaseAuth = FirebaseAuth.getInstance();
        userId = User.getID(context);
        username = User.getUsername(context);

        initializeUI();

        getAllChat();
    }

    private void initializeUI() {
        recyclerView = findViewById(R.id.rv_chat);
        etMessage = findViewById(R.id.et_chat_new_message);
        btnSend = findViewById(R.id.btn_chat_send);

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                message = etMessage.getText().toString();

                sendMessage();
                etMessage.setText("");
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.btn_chat_logout)
            openLogoutDialog();

        return super.onOptionsItemSelected(item);
    }

    private void getAllChat() {
        chatRef = FirebaseDatabase.getInstance().getReference("Chats");
        chatListener = chatRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                chatArrayList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    Chat chat = snapshot.getValue(Chat.class);
                    assert chat != null;
                    chatArrayList.add(chat);
                }

                Collections.sort(chatArrayList, Chat.ChatComparator);

                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
                linearLayoutManager.setReverseLayout(true);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.setHasFixedSize(true);
                chatAdapter = new ChatAdapter(context, chatArrayList, userId);
                recyclerView.setAdapter(chatAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void sendMessage() {
        long timeStamp = System.currentTimeMillis() / 1000;
        Date datetime = Calendar.getInstance().getTime();
        String chatId = UUID.randomUUID().toString() + timeStamp;

        chatRef = FirebaseDatabase.getInstance().getReference("Chats").child(chatId);

        HashMap<String, Object> conversationHashMap = new HashMap<>();
        conversationHashMap.put("id", chatId);
        conversationHashMap.put("username", username);
        conversationHashMap.put("userId", userId);
        conversationHashMap.put("message", message);
        conversationHashMap.put("datetime", datetime);

        chatRef.setValue(conversationHashMap);
    }

    private void openLogoutDialog() {
        AlertDialog dialog = new AlertDialog.Builder(context)
                .setTitle("Logout")
                .setIcon(R.drawable.ic_logout_lightgreen)
                .setMessage("Are you sure you want to logout?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logoutUser();
                    }
                })
                .setNegativeButton("NO", null)
                .create();
        dialog.show();
    }

    private void logoutUser() {
        firebaseAuth.signOut();
        User.clearSession(context);

        checkUserSession(true);
    }

    private void checkUserSession(boolean fromLogout) {
        String userId = User.getID(context);

        if (userId.isEmpty()) {
            if (fromLogout) {
                Intent intent = new Intent(context, SignUpActivity.class);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(context, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (chatListener != null)
            chatRef.removeEventListener(chatListener);
    }
}