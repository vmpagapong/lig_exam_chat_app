package com.example.lig_exam_chat_app.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.lig_exam_chat_app.R;
import com.example.lig_exam_chat_app.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {

    private Context context;

    private EditText etUsername, etPassword;
    private TextView tvUsernameError, tvPasswordError, tvSignup;
    private Button btnLogin;
    private ProgressBar progressBar;

    private String username, password;

    private FirebaseAuth firebaseAuth;
    private DatabaseReference userRefs;
    private ValueEventListener userListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = this;

        firebaseAuth = FirebaseAuth.getInstance();

        initializeUI();
    }

    private void initializeUI() {
        etUsername = findViewById(R.id.et_login_username);
        etPassword = findViewById(R.id.et_login_password);
        tvUsernameError = findViewById(R.id.tv_login_username_error);
        tvPasswordError = findViewById(R.id.tv_login_password_error);
        tvSignup = findViewById(R.id.tv_login_signup);
        btnLogin = findViewById(R.id.btn_login_login);
        progressBar = findViewById(R.id.progressBar_login);

        etUsername.requestFocus();

        etUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showError(false);
            }
        });

        etPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showError(false);
            }
        });

        tvSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SignUpActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                username = etUsername.getText().toString();
                password = etPassword.getText().toString();

                if (!areFieldsEmpty()) {
                    if (isLengthValid())
                        doLogin();
                    else
                        showError(true);
                } else
                    showError(true);
            }
        });
    }

    private void showProgress(boolean show) {
        progressBar.setVisibility(show ? View.VISIBLE : View.GONE);
        btnLogin.setVisibility(show ? View.GONE : View.VISIBLE);
        etUsername.setEnabled(!show);
        etPassword.setEnabled(!show);
        tvSignup.setEnabled(!show);
    }

    private void showError(boolean show) {
        tvUsernameError.setVisibility(show ? View.VISIBLE : View.GONE);
        tvPasswordError.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private boolean isLengthValid() {
        if (username.length() >= 8 && username.length() <= 16) {
            if (password.length() >= 8 && password.length() <= 16) {
                return true;
            }
        }

        return false;
    }

    private boolean areFieldsEmpty() {
        return username.isEmpty()
                && password.isEmpty();
    }

    private void doLogin() {
        showProgress(true);
        firebaseAuth.signInAnonymously().addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    userRefs = FirebaseDatabase.getInstance().getReference("Users");
                    userListener = userRefs.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            showProgress(false);
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                User userFirebase = snapshot.getValue(User.class);
                                assert userFirebase != null;
                                if (userFirebase.getUsername().equals(username)) {
                                    if (userFirebase.getPassword().equals(password)) {
                                        String userId = userFirebase.getId();

                                        User user = new User();
                                        user.setId(userId);
                                        user.setUsername(username);
                                        user.setPassword(password);
                                        user.saveUserSession(context);

                                        Intent intent = new Intent(context, ChatActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                }
                            }

                            showError(true);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                } else {
                    showProgress(false);
                    Toast.makeText(context, "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (userListener != null)
            userRefs.removeEventListener(userListener);
    }
}
