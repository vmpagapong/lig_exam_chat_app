package com.example.lig_exam_chat_app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.lig_exam_chat_app.activities.LoginActivity;
import com.example.lig_exam_chat_app.activities.SignUpActivity;

public class MainActivity extends AppCompatActivity {

    private Context context;

    private Button btnSignup, btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (getSupportActionBar() != null)
            getSupportActionBar().hide();

        context = this;

        initializeUI();
    }

    private void initializeUI() {
        btnSignup = findViewById(R.id.btn_main_signup);
        btnLogin = findViewById(R.id.btn_main_login);

        btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, SignUpActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
