package com.example.lig_exam_chat_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lig_exam_chat_app.R;
import com.example.lig_exam_chat_app.models.Chat;

import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {

    private static final int MSG_TYPE_LEFT = 0;
    private static final int MSG_TYPE_RIGHT = 1;

    private String userId;

    private Context context;
    private LayoutInflater layoutInflater;
    private ArrayList<Chat> chatArrayList;

    public ChatAdapter(Context context, ArrayList<Chat> chatArrayList, String userId) {
        this.context = context;
        this.chatArrayList = chatArrayList;
        this.layoutInflater = LayoutInflater.from(context);
        this.userId = userId;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        layoutInflater = LayoutInflater.from(context);
        if (i == MSG_TYPE_RIGHT) {
            View view = layoutInflater.inflate(R.layout.listrow_chat_right, viewGroup, false);
            return new ViewHolder(view);
        } else {
            View view = layoutInflater.inflate(R.layout.listrow_chat_left, viewGroup, false);
            return new ViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Chat chat = chatArrayList.get(i);
        String username;

        if (!chat.getUserId().equals(userId))
            username = chat.getUsername();
        else
            username = "You";

        viewHolder.tvUsername.setText(username);
        viewHolder.tvMessage.setText(chat.getMessage());

    }

    @Override
    public int getItemCount() {
        return chatArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView ivProfile;
        TextView tvMessage, tvUsername;

        public ViewHolder(View view) {
            super(view);

            tvMessage = view.findViewById(R.id.tv_listrow_chat_message);
            tvUsername = view.findViewById(R.id.tv_listrow_chat_username);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (chatArrayList.get(position).getUserId().equals(userId))
            return MSG_TYPE_RIGHT;
        else
            return MSG_TYPE_LEFT;
    }
}
