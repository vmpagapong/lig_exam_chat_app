package com.example.lig_exam_chat_app.models;

import java.util.Comparator;
import java.util.Date;

public class Chat {
    public String id;
    public String username;
    private String userId;
    public String message;
    public Date datetime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDatetime() {
        return datetime;
    }

    public void setDatetime(Date datetime) {
        this.datetime = datetime;
    }

    public static Comparator<Chat> ChatComparator = new Comparator<Chat>() {
        @Override
        public int compare(Chat o1, Chat o2) {
            Date dateCreated1 = o1.getDatetime();
            Date dateCreated2 = o2.getDatetime();

            return dateCreated2.compareTo(dateCreated1);
        }
    };
}
